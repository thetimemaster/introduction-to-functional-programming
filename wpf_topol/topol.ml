(*******************************
 ** Autor:   Piotr Kowalewski **
 ** Reviewer: Jagoda Kamińska **
 *******************************)

(* Sortowanie topologiczne *)

(* wyjatek rzucany przez [topol] gdy zaleznosci sa cykliczne *)
exception Cykliczne

(* Dodaje wszystkie krawędzie z jednej listy na PMap-e [graph] która dla ID wierzchołka
trzyma listę ID wierzchołków do których wychodzi z niego krawędź *)
let add_batch graph (id, lst) =
    let state = if PMap.exists id graph then PMap.find id graph else [] in
    let f = fun acc el -> el::acc in
    let new_state = List.fold_left f state lst in
    PMap.add id new_state graph

(* Dodaje wszystkie krawędzie z jednej listy na PMap-e [info] która dla ID wierzchołka 
trzyma krotkę (czy odwiedzony * liczbę krawędzi wchodzących do niego) *)
let add_info info (id, lst) =
    let id_info = if PMap.exists id info then PMap.find id info else (0, 0) in
    let info = PMap.add id id_info info in
    let f = fun info el -> 
            let (was, cnt) = if PMap.exists el info then PMap.find el info else (0, 0) in
            PMap.add el (was, cnt + 1) info in
    List.fold_left f info lst

(* DFS ogonowo, wchodzi wszędzie gdzie topologicznie może w danym stanie
grafu dodając wierzchołki do out *)
let rec dfs stack (info, graph, out) = 
    if stack = [] then (info, graph, out) else
    let id = List.hd stack in
    let (was,cnt) = PMap.find id info in
    let out = if was = 1 then out else id::out in
    let info = PMap.add id (1, 0) info in
    let next = if PMap.exists id graph then PMap.find id graph else [] in
    match next with
        | []   -> dfs (List.tl stack) (info, graph, out)
        | h::t -> 
            let graph = PMap.add id t graph in
            let (was, cnt) = PMap.find h info in
            let info = PMap.add h (was, cnt - 1) info in
            if cnt = 1 then dfs (h::stack) (info, graph, out)
            else dfs stack (info, graph, out)  

(* Funkcja do foldi, bierze aktualne ID wierzchołka z info i jeśli jeszcze w nim nie był
oraz nie ma krawędzi wchodzących których jeszcze nie przeszedł to zwraca wynik odpalonego
w nim dfsa, w przecinym wypadku zwraca obezny stan grafu *)
let starter = fun id _ (info, graph, out) -> 
    let (was, cnt) = PMap.find id info in
    if cnt > 0 || was = 1 then (info, graph, out)
    else dfs (id::[]) (info, graph, out) 

(* Dla danej listy [(a_1,[a_11;...;a_1n]); ...; (a_m,[a_m1;...;a_mk])] 
zwraca liste, na ktorej kazdy z elementow a_i oraz a_ij wystepuje
dokladnie raz i ktora jest uporzadkowana w taki sposob, ze kazdy
element a_i jest przed kazdym z elementow a_i1 ... a_il *)
let topol graph_list =
    let graph = List.fold_left add_batch PMap.empty graph_list in
    let info = List.fold_left add_info PMap.empty graph_list in
    let (info, graph, out) = PMap.foldi starter info (info, graph, []) in
    let f = fun (was,cnt) acc -> if was = 0 then true else acc in
    let cyclic = PMap.fold f info false in
    if cyclic then raise Cykliczne else (List.rev out)
