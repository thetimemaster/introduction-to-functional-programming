(*
 * PSet - Polymorphic sets
 * Copyright (C) 1996-2003 Xavier Leroy, Nicolas Cannasse, Markus Mottl
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version,
 * with the special exception on linking described in file LICENSE.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *)


open List;;

(* Typ zbioru elementów typu 'k z dodatkowym parametrem sumowanym:
 zbiór mniejszych * element * zbiór większych * wysokość * suma dodatkowego parametru w zbiorze*)
module SumSet = struct
	type 'k sum_set =
		| Empty
		| Node of 'k sum_set * 'k * 'k sum_set * int * int

	(* Definica zbioru z porządkiem*)
	type 'k t =
		{
			cmp : 'k -> 'k -> int;
			sum_set : 'k sum_set;
		}

	(* Suma dodatkowych parametrów w zbiorze *)
	let sum = function
		| Node (_, _, _, _, s) -> s
		| Empty -> 0
		
	(* Dodatkowy parametr elementu w korzeniu *)
	let spar = function
		| Node (l, _, r, _, s) -> s - (sum l) - (sum r)
		| Empty -> 0

	(* Wysokość drzewa BST na którym trzymany jest zbiór *)
	let height = function
		| Node (_, _, _, h, _) -> h
		| Empty -> 0


	(* Tworzy zbiór ze zbioru mniejszych, elementu z parametrem i zbioru większych *)
	let make l k par r = Node (l, k, r, max (height l) (height r) + 1, (sum l) + par + (sum r))

	(*Tworzy zbiór sumy mniejszych, elementu z parametrem i większych, zakłada różnice wysokości
	 l i r nie większą niż 3, wynikowe drzewo ma różnicę wysokości nie większą niż 2 *)
	let bal l k par r =
		let hl = height l in
		let hr = height r in
		if hl > hr + 2 then
			match l with
			| Node (ll, lk, lr, _, sl) ->
					if height ll >= height lr then make ll lk (spar l) (make lr k par r)
					else
						(match lr with
						| Node (lrl, lrk, lrr, _, slr) ->
								make (make ll lk (spar l) lrl) lrk (spar lr) (make lrr k par r)
						| Empty -> assert false)
			| Empty -> assert false
		else if hr > hl + 2 then
			match r with
			| Node (rl, rk, rr, _, sr) ->
					if height rr >= height rl then make (make l k par rl) rk (spar r) rr
					else
						(match rl with
						| Node (rll, rlk, rlr, _, srl) ->
								make (make l k par rll) rlk (spar rl) (make rlr rk (spar r) rr)
						| Empty -> assert false)
			| Empty -> assert false
		else Node (l, k, r, max hl hr + 1, (sum l) + par + (sum r))

	(* Zwraca najmniejszy element w zbiorze *)
	let rec min_elt = function
		| Node (Empty, k, r, _, s) -> (k,s - (sum r)) 
		| Node (l, _, _, _, _) -> min_elt l
		| Empty -> raise Not_found
		
		(* Zwraca najmniejszy element w zbiorze *)
	let rec max_elt = function
		| Node (l, k, Empty, _, s) -> (k,s - (sum l)) 
		| Node (_, _, r, _, _) -> max_elt r
		| Empty -> raise Not_found

	(* Zwraca zbiór bez bajmniejszego elementu *)
	let rec remove_min_elt = function
		| Node (Empty, _, r, _, _) -> r
		| Node (l, k, r, _, s) -> bal (remove_min_elt l) k (s - (sum l) - (sum r)) r
		| Empty -> invalid_arg "PSet.remove_min_elt"

	let merge t1 t2 =
		match t1, t2 with
		| Empty, _ -> t2
		| _, Empty -> t1
		| _ ->
				let (k, par) = min_elt t2 in
				bal t1 k par (remove_min_elt t2)

	(* Stworzenie pustego zbioru z porządkiem *)
	let create cmp = { cmp = cmp; sum_set = Empty }

	(* Stworzenie pustego zbioru z domyślnym porządkiem *)
	let empty = { cmp = compare; sum_set = Empty }

	(* Sprawdzenie czy zbiór jest pusty *)
	let is_empty x = 
		x.sum_set = Empty

	(* Dodaje element x z parametrem par z użyciem porządku x do danego zbioru *)
	let rec add_one cmp x par = function
		| Node (l, k, r, h, s) ->
				let c = cmp x k in
				if c = 0 then Node (l, x, r, h, par + (sum l) + (sum r))
				else if c < 0 then
					let nl = add_one cmp x par l in
					bal nl k (s - (sum l) - (sum r)) r
				else
					let nr = add_one cmp x par r in
					bal l k (s - (sum l) - (sum r)) nr
		| Empty -> Node (Empty, x, Empty, 1, par)

	(* Dodaje element x z parametrem par do danego zbioru z porządiem *)
	let add x par { cmp = cmp; sum_set = sum_set } =
		{ cmp = cmp; sum_set = add_one cmp x par sum_set }

	(* Łączy zbiór elementów mniejszych i większych elementem v z parametrem *)
	let rec join cmp l v par r =
		match (l, r) with
			(Empty, _) -> add_one cmp v par r
		| (_, Empty) -> add_one cmp v par l
		| (Node(ll, lv, lr, lh, sl), Node(rl, rv, rr, rh, sr)) ->
				if lh > rh + 2 then bal ll lv (spar l) (join cmp lr v par r) else
				if rh > lh + 2 then bal (join cmp l v par rl) rv (spar r) rr else
				make l v par r

	(* Zwraca trójkę zbiór mniejszych * czy element jest w zbiorze * zbiór większych *)
	let split x { cmp = cmp; sum_set = sum_set } =
		let rec loop x = function
				Empty ->
					(Empty, false, Empty)
			| Node (l, v, r, _, s) ->
					let c = cmp x v in
					if c = 0 then (l, true, r)
					else if c < 0 then
						let (ll, pres, rl) = loop x l in (ll, pres, join cmp rl v (s - (sum l) - (sum r)) r)
					else
						let (lr, pres, rr) = loop x r in (join cmp l v (s - (sum l) - (sum r)) lr, pres, rr)
		in
		let setl, pres, setr = loop x sum_set in
		{ cmp = cmp; sum_set = setl }, pres, { cmp = cmp; sum_set = setr }


	(* Usuwa x ze zboru *)
	let remove x { cmp = cmp; sum_set = sum_set } =
		let rec loop = function
			| Node (l, k, r, _, s) ->
			let par = s - (sum l) - (sum r) in
					let c = cmp x k in
					if c = 0 then merge l r else
					if c < 0 then bal (loop l) k par r else bal l k par (loop r)
			| Empty -> Empty in
		{ cmp = cmp; sum_set = loop sum_set }

	(* Sprawdza, czy x jest w zbiorze *)
	let mem x { cmp = cmp; sum_set = sum_set } =
		let rec loop = function
			| Node (l, k, r, _, _) ->
					let c = cmp x k in
					c = 0 || loop (if c < 0 then l else r)
			| Empty -> false in
		loop sum_set

	(* Alias dla mem *)
	let exists = mem

	(* ??? *)
	let iter f { sum_set = sum_set } =
		let rec loop = function
			| Empty -> ()
			| Node (l, k, r, _, s) -> loop l; f k (s - (sum l) - (sum r)); loop r in
		loop sum_set

	(* No fold no *)
	let fold f { cmp = cmp; sum_set = sum_set } acc =
		let rec loop acc = function
			| Empty -> acc
			| Node (l, k, r, _, s) ->
						loop (f k (s - (sum l) - (sum r)) (loop acc l)) r in
		loop acc sum_set

	(* Zwraca listę elementów *)
	let elements { sum_set = sum_set } = 
		let rec loop acc = function
				Empty -> acc
			| Node(l, k, r, _, _) -> loop (k :: loop acc r) l in
		loop [] sum_set 

end

module ISet = struct

type iSet = 
{
	lower: int SumSet.t;
	upper: int SumSet.t;
}

let print_ilist lst = print_string("[");iter (fun a -> print_int(a);print_string(" ")) lst;print_string("]");print_newline();lst

let comparer (a: int) (b:int) = 
	if a = b then 0 else
	if a < b then -1 else 1

let empty = 
{
	lower = SumSet.empty;
	upper = SumSet.empty;
}

let is_empty iset = SumSet.is_empty iset.lower

let elements iset = 
	let lw = SumSet.elements iset.lower
	and up = SumSet.elements iset.upper in 
		let mrg = fun h1 h2 acc -> (h1,h2)::acc in
			fold_right2 mrg lw up []
			
let fold f iset acc = 
	let lst = List.rev (elements iset) in
		fold_right f acc lst 
		
let iter f iset = 
	let lst = elements iset in
		iter f lst
		
let printpair (a,b) : unit = 
	print_string("(");print_int(a);print_string(",");print_int(b);print_string(")")
	
let printset iset = 
	iter printpair iset;print_newline();iset

let add (a,b) iset = 
	let (lpA,ipA,rpA) = SumSet.split a iset.lower
	and (lkA,ikA,rkA) = SumSet.split (a - 1) iset.upper 
	and (lpB,ipB,rpB) = SumSet.split (b + 1) iset.lower
	and (lkB,ikB,rkB) = SumSet.split b iset.upper in
		let lw = {SumSet.cmp = lpA.cmp; sum_set = SumSet.merge lpA.sum_set rpB.sum_set} 
		and up = {SumSet.cmp = lkA.cmp; sum_set = SumSet.merge lkA.sum_set rkB.sum_set} in
			let shall_add_lw = SumSet.is_empty lpA || (not (SumSet.is_empty lkA) && fst (SumSet.max_elt lpA.sum_set) <= fst (SumSet.max_elt lkA.sum_set))
			and shall_add_up = SumSet.is_empty rkB || (not (SumSet.is_empty rpB) && fst (SumSet.min_elt rkB.sum_set) >= fst (SumSet.min_elt rpB.sum_set)) in
				let lw_c = if shall_add_lw then SumSet.add a (a - 1) lw else lw
				and up_c = if shall_add_up then SumSet.add b b up else up
					in {lower = lw_c; upper = up_c}
				
let remove (a,b) iset = 
	let (lpA,ipA,rpA) = SumSet.split a iset.lower
	and (lkA,ikA,rkA) = SumSet.split a iset.upper 
	and (lpB,ipB,rpB) = SumSet.split b iset.lower
	and (lkB,ikB,rkB) = SumSet.split b iset.upper in
		let lw = {SumSet.cmp = lpA.cmp; sum_set = SumSet.merge lpA.sum_set rpB.sum_set} 
		and up = {SumSet.cmp = lkA.cmp; sum_set = SumSet.merge lkA.sum_set rkB.sum_set} in
			let shall_leave_up = SumSet.is_empty lpA || (not (SumSet.is_empty lkA) && fst (SumSet.max_elt lpA.sum_set) <= fst (SumSet.max_elt lkA.sum_set))
			and shall_leave_lw = SumSet.is_empty rkB || (not (SumSet.is_empty rpB) && fst (SumSet.min_elt rkB.sum_set) >= fst (SumSet.min_elt rpB.sum_set)) in
			let up_c = if shall_leave_up then up else SumSet.add (a - 1) (a - 1) up
			and lw_c = if shall_leave_lw then lw else SumSet.add (b + 1) b lw
				in {lower = lw_c; upper = up_c}
		
		
let mem a iset = 
	if is_empty iset then false else
	let (lp,ip,rp) = SumSet.split a iset.lower
	and (lk,ik,rk) = SumSet.split a iset.upper in
		if ip || ik then true
		else if (SumSet.is_empty rk) || (SumSet.is_empty lp) then false
		else if SumSet.is_empty rp then true
		else (fst (SumSet.min_elt rk.sum_set) < fst (SumSet.min_elt rp.sum_set))
		
let below a iset = 
	let (lp,ip,rp) = SumSet.split (a + 1) iset.lower
	and (lk,ik,rk) = SumSet.split (a + 1) iset.upper in
		let sm = (SumSet.sum lk.sum_set) - (SumSet.sum lp.sum_set) in
			let ost = if (mem (a + 1) iset) && (mem a iset) then sm + a else sm in
				if ost < 0 then max_int else ost

let split a iset = 
	let (lp,ip,rp) = SumSet.split a iset.lower
	and (lk,ik,rk) = SumSet.split a iset.upper in
		if mem a iset then
			let nu = if ip then lk else SumSet.add (a - 1) (a - 1) lk
			and nl = if ik then rp else SumSet.add (a + 1) a rp in
				{lower = lp; upper = nu},true,{lower = nl; upper = rk}
		else
			{lower = lp; upper = lk},false,{lower = rp; upper = rk}

end


let a0 = ISet.empty;;
let a1 = ISet.add (1,5) a0;;
ISet.iter ISet.printpair a1;print_newline();;
let a2 = ISet.add (6,7) a1;;
ISet.iter ISet.printpair a2;print_newline();;
let a3 = ISet.add (10,11) a2;;
ISet.iter ISet.printpair a3;print_newline();;
let a4 = ISet.add (14,17) a3;;
ISet.iter ISet.printpair a4;print_newline();;
let a5 = ISet.add (4,12) a4;;
ISet.iter ISet.printpair a5;print_newline();;
let a6 = ISet.remove (3,15) a5;;
ISet.iter ISet.printpair a6;;print_newline();;


