(* Autor: Piotr Kowalewski, recenzent: Damian Burczyk *)
(*
 * PSet - Polymorphic sets
 * Copyright (C) 1996-2018 Xavier Leroy, Nicolas Cannasse, Markus Mottl, Jacek Chrząszcz, Piotr Kowalewski
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version,
 * with the special exception on linking described in file LICENSE.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *)

(* Pomocniczy moduł, zbiór elementów dowolnego typu z parametrem typu int, pozwala na 
  operacje z pSeta oraz na sprawdzenie sumy parametrów wszystkich elementów w zbiorze*)
module SumSet = struct

  (* Typ zbioru elementów typu 'k z dodatkowym parametrem typu int, reprezentowany na
  drzewie AVL:
  zbiór mniejszych * element * zbiór większych * wysokość * suma dodatkowych parametrów *)
  type 'k sum_set =
    | Empty
    | Node of 'k sum_set * 'k * 'k sum_set * int * int

  (* Zbiór sum_set ze zdefiniowanym porządkiem cmp*)
  type 'k t =
  {
    cmp : 'k -> 'k -> int;
    sum_set : 'k sum_set;
  }

  (* Suma dodatkowych parametrów elementów w zbiorze *)
  let sum = function
    | Node (_, _, _, _, s) -> s
    | Empty -> 0
    
  (* Dodatkowy parametr elementu w korzeniu drzewa reprezentującego zbiór *)
  let spar = function
    | Node (l, _, r, _, s) -> s - (sum l) - (sum r)
    | Empty -> 0

  (* Wysokość drzewa reprezentującego zbiór *)
  let height = function
    | Node (_, _, _, h, _) -> assert(h<50);h
    | Empty -> 0

  (* Tworzy zbiór ze zbioru mniejszych [l], elementu [k] z parametrem [par] i zbioru większych 
  [r], nie balansuje, drzew w reprezentacjach (l i r powinny nie różnic sie wysokoscią o więcej
  niż 2) *)
  let make l k par r = Node (l, k, r, max (height l) (height r) + 1, (sum l) + par + (sum r))

  (* Łączy zbiór mniejszych [l], element [k] z parametrem [par] i zbiór większych [r], zakłada 
   różnice wysokości [l] i [r] nie większą niż 3, wynikowe drzewo ma różnicę wysokości nie 
   większą niż 2 *)
  let bal l k par r =
    let hl = height l in
    let hr = height r in
    if hl > hr + 2 then
      match l with
      | Node (ll, lk, lr, _, _) ->
          if height ll >= height lr then make ll lk (spar l) (make lr k par r)
          else
            (match lr with
            | Node (lrl, lrk, lrr, _, _) ->
                make (make ll lk (spar l) lrl) lrk (spar lr) (make lrr k par r)
            | Empty -> assert false)
      | Empty -> assert false
    else if hr > hl + 2 then
      match r with
      | Node (rl, rk, rr, _, _) ->
          if height rr >= height rl then make (make l k par rl) rk (spar r) rr
          else
            (match rl with
            | Node (rll, rlk, rlr, _, _) ->
                make (make l k par rll) rlk (spar rl) (make rlr rk (spar r) rr)
            | Empty -> assert false)
      | Empty -> assert false
    else Node (l, k, r, max hl hr + 1, (sum l) + par + (sum r))

  (* Zwraca parę (najmniejszy element w zbiorze, jego parametr)*)
  let rec min_elt = function
    | Node (Empty, k, r, _, s) -> (k,s - (sum r)) 
    | Node (l, _, _, _, _) -> min_elt l
    | Empty -> raise Not_found
    
  (* Zwraca parę (największy element w zbiorze, jego parametr) *)
  let rec max_elt = function
    | Node (l, k, Empty, _, s) -> (k,s - (sum l)) 
    | Node (_, _, r, _, _) -> max_elt r
    | Empty -> raise Not_found

  (* Zwraca zbiór bez najmniejszego elementu *)
  let rec remove_min_elt = function
    | Node (Empty, _, r, _, _) -> r
    | Node (l, k, r, _, s) -> bal (remove_min_elt l) k (s - (sum l) - (sum r)) r
    | Empty -> invalid_arg "SumSet.remove_min_elt"

  (* Łączy zbiory [t1] i [t2], zakłada max(t1)<min(t2), zakłąda różnicę wysokosci max 2 *)
  let merge t1 t2 =
    match t1, t2 with
    | Empty, _ -> t2
    | _, Empty -> t1
    | _ ->
        let (k, par) = min_elt t2 in
        bal t1 k par (remove_min_elt t2)

  (* Stworzenie pustego zbioru z porządkiem *)
  let create cmp = { cmp = cmp; sum_set = Empty }

  (* Stworzenie pustego zbioru z domyślnym porządkiem *)
  let empty = { cmp = compare; sum_set = Empty }

  (* Sprawdzenie czy zbiór jest pusty *)
  let is_empty x = 
    x.sum_set = Empty

  (* Dodaje element [x] z parametrem par z użyciem porządku [x] do danego zbioru *)
  let rec add_one cmp x par = function
    | Node (l, k, r, h, s) ->
        let c = cmp x k in
        if c = 0 then Node (l, x, r, h, par + (sum l) + (sum r))
        else if c < 0 then
          let nl = add_one cmp x par l in
          bal nl k (s - (sum l) - (sum r)) r
        else
          let nr = add_one cmp x par r in
          bal l k (s - (sum l) - (sum r)) nr
    | Empty -> Node (Empty, x, Empty, 1, par)

  (* Dodaje element [x] z parametrem [par] do danego zbioru z porządiem *)
  let add x par { cmp = cmp; sum_set = sum_set } =
    { cmp = cmp; sum_set = add_one cmp x par sum_set }

  (* Łączy zbiór elementów mniejszych [l] i większych [r] z elementem [v] o parametre [par]*)
  let rec join cmp l v par r =
    match (l, r) with
      (Empty, _) -> add_one cmp v par r
    | (_, Empty) -> add_one cmp v par l
    | (Node(ll, lv, lr, lh, sl), Node(rl, rv, rr, rh, sr)) ->
        if lh > rh + 2 then bal ll lv (spar l) (join cmp lr v par r) else
        if rh > lh + 2 then bal (join cmp l v par rl) rv (spar r) rr else
        make l v par r

  (* Zwraca trójkę zbiór mniejszych * czy element [x] jest w zbiorze * zbiór większych *)
  let split x { cmp = cmp; sum_set = sum_set } =
    let rec loop x = function
        Empty ->
          (Empty, false, Empty)
    | Node (l, v, r, _, s) ->
          let c = cmp x v in
          if c = 0 then (l, true, r)
          else if c < 0 then
            let (ll, pres, rl) = loop x l in (ll, pres, join cmp rl v (s - (sum l) - (sum r)) r)
          else
            let (lr, pres, rr) = loop x r in (join cmp l v (s - (sum l) - (sum r)) lr, pres, rr)
    in
    let setl, pres, setr = loop x sum_set in
    { cmp = cmp; sum_set = setl }, pres, { cmp = cmp; sum_set = setr }

  (* Zwraca zbiów bez elementu [x] *)
  let remove x { cmp = cmp; sum_set = sum_set } =
    let rec loop = function
      | Node (l, k, r, _, s) ->
      let par = s - (sum l) - (sum r) in
          let c = cmp x k in
          if c = 0 then merge l r else
          if c < 0 then bal (loop l) k par r else bal l k par (loop r)
      | Empty -> Empty in
    { cmp = cmp; sum_set = loop sum_set }

  (* Sprawdza, czy [x] jest w zbiorze *)
  let mem x { cmp = cmp; sum_set = sum_set } =
    let rec loop = function
      | Node (l, k, r, _, _) ->
          let c = cmp x k in
          c = 0 || loop (if c < 0 then l else r)
      | Empty -> false in
    loop sum_set

  (* Zwraca listę elementów w zbiorze*)
  let elements { sum_set = sum_set } = 
    let rec loop acc = function
        Empty -> acc
      | Node(l, k, r, _, _) -> loop (k :: loop acc r) l in
    loop [] sum_set 

end

(* Typ zbioru przedziałów liczb całkowitych 
lower: SumSet elementów [a] typu int z parametrem [a-1]: początki kolejnych przedziałów
upper: SumSet elementów [b] typu int z parametrem [b]  : konce kolejnych przedziałów *) 
type t = 
{
  lower: int SumSet.t;
  upper: int SumSet.t;
}

(* Zwiększa liczbę o 1 o ile nie zawinie to jej poza zakres *)
let incc a = 
  if a = max_int then a
  else a + 1

(* Zmniejsza liczbę o 1 o ile nie zawinie to jej poza zakres *)
let decc a = 
  if a = min_int then a
  else a - 1

(* Pusty zbiów przedziałów *)
let empty = 
{
  lower = SumSet.empty;
  upper = SumSet.empty;
}

(* Sprawdza czy zbiór [iset] jest pusty *)
let is_empty iset = SumSet.is_empty iset.lower

(* Zwraca listę przedziałów w [iset] w kolejnosci rosnącej *)
let elements iset = 
  let lw = List.rev(SumSet.elements iset.lower)
  and up = List.rev(SumSet.elements iset.upper) in 
  let mrg = fun acc h1 h2 -> (h1,h2)::acc in
  List.fold_left2 mrg [] lw up

(* Oblicza f(xN ... f(x2 f(x1 acc)) ... )  gdzie x1...xN to przedziały w [iset]*)
let fold f iset acc = 
  let lst = elements iset in
  let f2 = fun a b -> f b a in
  List.fold_left f2 acc lst 

(* Używa f na wszystkich przedziałach w iset w kolejnosci rosnącej *)
let iter f iset = 
  let lst = elements iset in
  List.iter f lst

(* Zwraca [iset] U (a,b) *)
let add (a,b) iset = 
  let open SumSet in
    let (lpA, _, _) = split a iset.lower
    and (lkA, _, _) = split (decc a) iset.upper 
    and ( _, _,rpB) = split (incc b) iset.lower
    and ( _, _,rkB) = split b iset.upper in
    let lw = {cmp = lpA.cmp; sum_set = merge lpA.sum_set rpB.sum_set} 
    and up = {cmp = lkA.cmp; sum_set = merge lkA.sum_set rkB.sum_set} in
    let shall_add_lw = is_empty lpA || (not (is_empty lkA) &&
      fst (max_elt lpA.sum_set) <= fst (max_elt lkA.sum_set))  
    and shall_add_up = is_empty rkB || (not (is_empty rpB) &&
      fst (min_elt rkB.sum_set) >= fst (min_elt rpB.sum_set)) in  
    let lw_c = if shall_add_lw then add a (a - 1) lw else lw
    and up_c = if shall_add_up then add b b up else up
    in {lower = lw_c; upper = up_c}

(* Zwraca [iset] \ (a,b) *)
let remove (a,b) iset = 
  let open SumSet in
    let (lpA, _, _) = split a iset.lower
    and (lkA, _, _) = split a iset.upper 
    and ( _, _,rpB) = split b iset.lower
    and ( _, _,rkB) = split b iset.upper in
    let lw = {cmp = lpA.cmp; sum_set = merge lpA.sum_set rpB.sum_set} 
    and up = {cmp = lkA.cmp; sum_set = merge lkA.sum_set rkB.sum_set} in
    let shall_leave_up = is_empty lpA || (not (is_empty lkA) && 
      fst (max_elt lpA.sum_set) <= fst (max_elt lkA.sum_set))  
    and shall_leave_lw = is_empty rkB || (not (is_empty rpB) &&
      fst (min_elt rkB.sum_set) >= fst (min_elt rpB.sum_set)) in  
    let up_c = if shall_leave_up then up else add (a - 1) (a - 1) up
    and lw_c = if shall_leave_lw then lw else add (b + 1) b lw
    in {lower = lw_c; upper = up_c}
    
(* Sprawdza czy [a] jest w [iset] *)
let mem a iset = 
  if is_empty iset then false else
  let (lp,ip,rp) = SumSet.split a iset.lower
  and (lk,ik,rk) = SumSet.split a iset.upper in
  if ip || ik then true
  else if (SumSet.is_empty rk) || (SumSet.is_empty lp) then false
  else if SumSet.is_empty rp then true
  else (fst (SumSet.min_elt rk.SumSet.sum_set) < fst (SumSet.min_elt rp.SumSet.sum_set))

(* Zwraca liczbę elementów w [iset] nie większych [a] *)  
let rec below a iset =
  if a = max_int then 
    if mem a iset then incc(below (a - 1) iset) 
    else below (a - 1) iset 
  else 
    let (lp,ip,rp) = SumSet.split (a + 1) iset.lower
    and (lk,ik,rk) = SumSet.split (a + 1) iset.upper in
    let sm = (SumSet.sum lk.SumSet.sum_set) - (SumSet.sum lp.SumSet.sum_set) in
    let ost = if (mem (a + 1) iset) && (mem a iset) then sm + a else sm in
    if ost < 0 then max_int else 
    if ost = 0 && mem a iset then max_int
    else ost

(* Zwraca trójkę:
(zbiór elementy mniejsze * czy [a] jest w [iset] * zbiór elementów większych) *)
let split a iset = 
  let (lp,ip,rp) = SumSet.split a iset.lower
  and (lk,ik,rk) = SumSet.split a iset.upper in
  if mem a iset then
    let nu = if ip then lk else SumSet.add (a - 1) (a - 1) lk
    and nl = if ik then rp else SumSet.add (a + 1) a rp in
    {lower = lp; upper = nu},true,{lower = nl; upper = rk}
  else
    {lower = lp; upper = lk},false,{lower = rp; upper = rk}
