(***************************
 * Autor: Piotr Kowalewski *
 * Reviewer:               *
 ***************************)


(** Punkt na płaszczyźnie *)
type point = float * float

(** Poskładana kartka: ile razy kartkę przebije szpilka wbita w danym
punkcie *)
type kartka = point -> int

let eps = 0.0000001

(** kwadrat odległosci 2 punktów *)
let dist (x1, y1) (x2, y2) = 
	(x1 -. x2) *. (x1 -. x2) +. (y1 -. y2) *. (y1 -. y2)

(** Zwraca wektor długosci 1 o tym samym kierunku *)
let normalise (x, y) = 
	let len = sqrt(dist (x, y) (0., 0.)) in
	(x /. len, y /. len)

let print_point (p_x,p_y) = 
	print_string("(");
	print_float(p_x);
	print_string(",");
	print_float(p_y);
	print_string(")");
	print_newline()

(** [prostokat p1 p2] zwraca kartkę, reprezentującą domknięty
prostokąt o bokach równoległych do osi układu współrzędnych i lewym
dolnym rogu [p1] a prawym górnym [p2]. Punkt [p1] musi więc być
nieostro na lewo i w dół od punktu [p2]. Gdy w kartkę tę wbije się 
szpilkę wewnątrz (lub na krawędziach) prostokąta, kartka zostanie
przebita 1 raz, w pozostałych przypadkach 0 razy *)
let prostokat (min_x, min_y) (max_x, max_y) = 
	fun (p_x, p_y) ->
		(*print_point((p_x,p_y));*)
		if p_x >= (min_x -. eps) && p_y >= (min_y -. eps) &&
		   p_x <= (max_x +. eps) && p_y <= (max_y +. eps) then 1 else 0

(** [kolko p r] zwraca kartkę, reprezentującą kółko domknięte o środku
w punkcie [p] i promieniu [r] *)
let kolko center radius = 
	fun p ->
		if sqrt(dist center p) <= (radius +. eps) then 1 else 0 

(** Zwraca 0 jesli punkt pc jest na prostej p1->p2, liczbę ujemną jesli jest po jej prawej
stronie lub liczbe dodatnią po lewej stronie **)
let side (p1_x, p1_y) (p2_x, p2_y) (pc_x, pc_y) = 
	(*print_string("side");print_point(p1_x, p1_y);print_point(p2_x, p2_y);print_point(pc_x, pc_y);
	print_string("=");*)
	(p2_x -. p1_x) *. (pc_y -. p1_y) -. (p2_y -. p1_y) *. (pc_x -. p1_x)

(** Odbja punkt pc względem prostej p1->p2 *)
let reverse (p1_x, p1_y) (p2_x, p2_y) (pc_x, pc_y) =
	(*print_point(p1_x, p1_y);
	print_point(p2_x, p2_y);
	print_point(pc_x, pc_y);*)
	let (np_x, np_y) = normalise (p2_x -. p1_x, p2_y -. p1_y) in
	let (nc_x, nc_y) = (pc_x -. p1_x, pc_y -. p1_y) in
	(*print_point(np_x, np_y);
	print_point(nc_x, nc_y);*)
	let len_rzut = (np_x *. nc_x) +. (np_y *. nc_y) in
	(*print_float(len_rzut);print_newline();*)
	let (rzut_x, rzut_y) = (len_rzut *. np_x, len_rzut *. np_y) in
	let a = (2. *. rzut_x -. nc_x +. p1_x, 2. *. rzut_y -. nc_y +. p1_y)in
	(*print_string("reverse\n");print_point(p1_x, p1_y);print_point(p2_x, p2_y);print_point(pc_x, pc_y);
	print_string("=\n");
	print_point(a);*)a

(** [zloz p1 p2 k] składa kartkę [k] wzdłuż prostej przechodzącej
przez punkty [p1] i [p2] (muszą to być różne punkty). Papier jest
składany w ten sposób, że z prawej strony prostej (patrząc w kierunku
od [p1] do [p2]) jest przekładany na lewą. Wynikiem funkcji jest
złożona kartka. Jej przebicie po prawej stronie prostej powinno więc
zwrócić 0. Przebicie dokładnie na prostej powinno zwrócić tyle samo,
co przebicie kartki przed złożeniem. Po stronie lewej - tyle co przed
złożeniem plus przebicie rozłożonej kartki w punkcie, który nałożył
się na punkt przebicia. *)

let nearzero s = 
	s< eps && s> (-.eps)

let zloz p1 p2 paper =
	fun p ->
		let s = side p1 p2 p in
		(*print_string("zloz");print_point(p);
		print_float(s);print_newline();*)
		if nearzero s then (paper p) else
		if s < 0. then 0 else
		let a1 = paper (reverse p1 p2 p) in
		let a2 = paper p in
		a1 + a2


(** [skladaj [(p1_1,p2_1);...;(p1_n,p2_n)] k = zloz p1_n p2_n (zloz ... (zloz p1_1 p2_1 k)...)] 
czyli wynikiem jest złożenie kartki [k] kolejno wzdłuż wszystkich prostych 
z listy *)
let skladaj points paper = 
	let r_zloz kart (p1,p2) = zloz p1 p2 kart in
	List.fold_left r_zloz paper points

