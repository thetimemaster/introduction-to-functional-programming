(***************************
 * Zadanie Arytmetyka      *
 * Autor: Piotr Kowalewski *
 * Reviewer: Juliusz Pham  *
 ***************************)

(* Typ złączalnej kolejki priorytetowej *)    
type 'a queue = 
    | Empty
    | Node of {
        value : 'a;
        left  : 'a queue;
        right : 'a queue;
        height: int}


(* Wyjątek podnoszony przez [delete_min] gdy kolejka jest pusta *)
exception Empty


(* Pusta kolejka priorytetowa *)
let empty : 'a queue = Empty


(* Tworzy jednoelementową kolejkę z danego elementu *)
let element_to_queue (element: 'a) : 'a queue = Node{
    value  = element;
    left   = Empty;
    right  = Empty;
    height = 1}


(* Zwraca najmniejszą wartość na kolejce bez usuwania jej *)
let get_min (target_queue : 'a queue) : 'a = 
    match target_queue with
        | Empty  -> raise Empty
        | Node n -> n.value


(* Zwraca minimalną prawą wysokość *)
let get_height (target_queue : 'a queue) : int = 
    match target_queue with
        | Empty  -> 0
        | Node n -> n.height


(* [join q1 q2] zwraca złączenie kolejek [q1] i [q2] *)
let rec join (queue_a: 'a queue) (queue_b: 'a queue) : 'a queue = 
    match (queue_a, queue_b) with
        | (Empty, Empty)   -> Empty
        | (Node a, Empty)  -> queue_a 
        | (Empty, Node b)  -> queue_b
        | (Node a, Node b) -> 
            (* Chce założyć, że korzeń A < korzeń B *)
            if a.value > b.value then join queue_b queue_a else 
                let rec_generated = join a.right queue_b in
                if get_height a.left > get_height rec_generated then Node {
                    value  = a.value;
                    left   = a.left;
                    right  = rec_generated;
                    height = get_height rec_generated + 1}
                else Node {
                    value  = a.value;
                    left   = rec_generated;
                    right  = a.left;
                    height = get_height a.left + 1}


(* [add e q] zwraca kolejkę powstałą z dołączenia elementu [e] do kolejki [q] *)
let add (element: 'a) (target_queue: 'a queue) : 'a queue = 
    let new_queue = element_to_queue element
    in match target_queue with
        | Empty  -> new_queue
        | Node n -> join target_queue new_queue


(* Dla niepustej kolejki [q], [delete_min q] zwraca parę [(e,q')] gdzie [e]
    jest elementem minimalnym kolejki [q] a [q'] to [q] bez elementu [e].
    Jeśli [q] jest puste podnosi wyjątek [Empty]. *)    
let delete_min target_queue = 
    match target_queue with 
        | Empty  -> raise Empty
        | Node n -> (n.value, join n.left n.right) 


(* Zwraca [true] jeśli dana kolejka jest pusta. W przeciwnym razie [false] *)
let is_empty (target_queue: 'a queue) : bool = 
    match target_queue with
        | Empty  -> true
        | Node _ -> false

